## 実装リスト
* DI (標準)
* AOP (AspectCore)
* Unit Test
* Authentication
* 外部APIへの接続(Firebaseメイン)
* Swagger(Swashbuckle)
* Lint
* Validation
* ErrorStatus

## コンテナ実装リスト
* Nginxとの連携
* Kubernetesとの連携
* Azure Kubernetes Serviceとの連携

## CI/CD実装リスト
* Gitlab-ciでAKSへアップロード