using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace exampleproject.Services
{
  public interface IWeatherForecastService
  {
      IEnumerable<WeatherForecast> GetWeatherForecasts();

      WeatherForecast GetWeatherForecast(int date);
  }

}